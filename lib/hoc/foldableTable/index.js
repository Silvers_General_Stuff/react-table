'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _left = require('./left.svg');

var _left2 = _interopRequireDefault(_left);

var _right = require('./right.svg');

var _right2 = _interopRequireDefault(_right);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint-disable */

var defaultFoldIconComponent = function defaultFoldIconComponent(_ref) {
  var collapsed = _ref.collapsed,
      header = _ref.header;

  var style = { width: 25 };

  if (collapsed) return _react2.default.createElement('img', { src: _right2.default, style: style, alt: 'right', title: header });
  return _react2.default.createElement('img', { src: _left2.default, style: style, alt: 'left' });
};

var defaultFoldButtonComponent = function defaultFoldButtonComponent(_ref2) {
  var header = _ref2.header,
      collapsed = _ref2.collapsed,
      icon = _ref2.icon,
      onClick = _ref2.onClick;

  var style = {
    marginLeft: '0px',
    marginTop: '-5px',
    marginBottom: '-8px',
    float: 'left',
    cursor: 'pointer'
  };

  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      'div',
      { style: style, onClick: onClick },
      icon
    ),
    !collapsed && _react2.default.createElement(
      'div',
      { style: { "textAlign": "left" } },
      header
    )
  );
};

exports.default = function (ReactTable) {
  var wrapper = function (_React$Component) {
    _inherits(RTFoldableTable, _React$Component);

    function RTFoldableTable(props, context) {
      _classCallCheck(this, RTFoldableTable);

      var _this = _possibleConstructorReturn(this, (RTFoldableTable.__proto__ || Object.getPrototypeOf(RTFoldableTable)).call(this, props, context));

      _this.onResizedChange = function (resized) {
        var onResizedChange = _this.props.onResizedChange;

        if (onResizedChange) onResizedChange(resized);else {
          _this.setState(function (p) {
            return { resized: resized };
          });
        }
      };

      _this.removeResized = function (column) {
        var id = column.id;

        if (!id) return;

        var resized = _this.state.resized;

        if (!resized) return;

        var rs = resized.find(function (r) {
          return r.id === id;
        });
        if (!rs) return;

        var newResized = resized.filter(function (r) {
          return r !== rs;
        });
        _this.onResizedChange(newResized);
      };

      _this.getWrappedInstance = function () {
        if (!_this.wrappedInstance) console.warn('RTFoldableTable - No wrapped instance');
        if (_this.wrappedInstance.getWrappedInstance) return _this.wrappedInstance.getWrappedInstance();
        return _this.wrappedInstance;
      };

      _this.getCopiedKey = function (key) {
        var foldableOriginalKey = _this.props.foldableOriginalKey;

        return '' + foldableOriginalKey + key;
      };

      _this.copyOriginals = function (column) {
        var FoldedColumn = _this.props.FoldedColumn;

        // Stop copy if the column already copied

        if (column.original_Header) return;

        Object.keys(FoldedColumn).forEach(function (k) {
          var copiedKey = _this.getCopiedKey(k);

          if (k === 'Cell') column[copiedKey] = column[k] ? column[k] : function (c) {
            return c.value;
          };else column[copiedKey] = column[k];
        });

        // Copy sub Columns
        if (column.columns && !column.original_Columns) column.original_Columns = column.columns;

        // Copy Header
        if (!column.original_Header) column.original_Header = column.Header;
      };

      _this.restoreToOriginal = function (column) {
        var FoldedColumn = _this.props.FoldedColumn;


        Object.keys(FoldedColumn).forEach(function (k) {
          // ignore header as handling by foldableHeaderRender
          if (k === 'Header') return;

          var copiedKey = _this.getCopiedKey(k);
          column[k] = column[copiedKey];
        });

        if (column.columns && column.original_Columns) column.columns = column.original_Columns;
      };

      _this.getState = function () {
        return _this.props.onFoldChange ? _this.props.folded : _this.state.folded;
      };

      _this.isFolded = function (col) {
        var folded = _this.getState();
        return folded[col.id] === true;
      };

      _this.foldingHandler = function (col) {
        if (!col || !col.id) return;

        var onFoldChange = _this.props.onFoldChange;

        var folded = _this.getState();
        var id = col.id;


        var newFold = Object.assign({}, folded);
        newFold[id] = !newFold[id];

        // Remove the Resized if have
        _this.removeResized(col);

        if (onFoldChange) onFoldChange(newFold);else {
          _this.setState(function (previous) {
            return { folded: newFold };
          });
        }
      };

      _this.foldableHeaderRender = function (cell) {
        var _this$props = _this.props,
            FoldButtonComponent = _this$props.FoldButtonComponent,
            FoldIconComponent = _this$props.FoldIconComponent;
        var column = cell.column;

        var header = column.original_Header;
        var collapsed = _this.isFolded(column);
        var icon = _react2.default.createElement(FoldIconComponent, { collapsed: collapsed, header: header });
        var onClick = function onClick() {
          return _this.foldingHandler(column);
        };

        return _react2.default.createElement(FoldButtonComponent, {
          header: header,
          collapsed: collapsed,
          icon: icon,
          onClick: onClick
        });
      };

      _this.applyFoldableForColumn = function (column) {
        var collapsed = _this.isFolded(column);
        var FoldedColumn = _this.props.FoldedColumn;

        // Handle Column Header

        if (column.columns) {
          if (collapsed) {
            column.columns = [FoldedColumn];
            column.width = FoldedColumn.width;
            column.style = FoldedColumn.style;
          } else _this.restoreToOriginal(column);
        }
        // Handle Normal Column.
        else if (collapsed) column = Object.assign(column, FoldedColumn);else {
            _this.restoreToOriginal(column);
          }
      };

      _this.applyFoldableForColumns = function (columns) {
        return columns.map(function (col, index) {
          if (!col.foldable) return col;

          // If col don't have id then generate id based on index
          if (!col.id) col.id = 'col_' + index;

          _this.copyOriginals(col);
          // Replace current header with internal header render.
          col.Header = function (c) {
            return _this.foldableHeaderRender(c);
          };
          // apply foldable
          _this.applyFoldableForColumn(col);

          // return the new column out
          return col;
        });
      };

      _this.state = {
        folded: props.onFoldChange ? undefined : {},
        resized: props.resized || []
      };
      return _this;
    }

    _createClass(RTFoldableTable, [{
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(newProps) {
        if (this.state.resized !== newProps.resized) {
          this.setState(function (p) {
            return { resized: newProps.resized };
          });
        }
      }

      // this is so we can expose the underlying ReactTable.

    }, {
      key: 'render',
      value: function render() {
        var _this2 = this;

        var _props = this.props,
            originalCols = _props.columns,
            FoldButtonComponent = _props.FoldButtonComponent,
            FoldIconComponent = _props.FoldIconComponent,
            FoldedColumn = _props.FoldedColumn,
            rest = _objectWithoutProperties(_props, ['columns', 'FoldButtonComponent', 'FoldIconComponent', 'FoldedColumn']);

        var columns = this.applyFoldableForColumns([].concat(_toConsumableArray(originalCols)));

        var extra = {
          columns: columns,
          onResizedChange: this.onResizedChange,
          resized: this.state.resized
        };

        return _react2.default.createElement(ReactTable, _extends({}, rest, extra, { ref: function ref(r) {
            return _this2.wrappedInstance = r;
          } }));
      }
    }]);

    return RTFoldableTable;
  }(_react2.default.Component);

  wrapper.displayName = 'RTFoldableTable';
  wrapper.defaultProps = {
    FoldIconComponent: defaultFoldIconComponent,
    FoldButtonComponent: defaultFoldButtonComponent,
    foldableOriginalKey: 'original_',
    FoldedColumn: {
      Cell: function Cell(c) {
        return '';
      },
      width: 30,
      sortable: false,
      resizable: false,
      filterable: false
    }
  };

  return wrapper;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9ob2MvZm9sZGFibGVUYWJsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJkZWZhdWx0Rm9sZEljb25Db21wb25lbnQiLCJjb2xsYXBzZWQiLCJoZWFkZXIiLCJzdHlsZSIsIndpZHRoIiwicmlnaHQiLCJsZWZ0IiwiZGVmYXVsdEZvbGRCdXR0b25Db21wb25lbnQiLCJpY29uIiwib25DbGljayIsIm1hcmdpbkxlZnQiLCJtYXJnaW5Ub3AiLCJtYXJnaW5Cb3R0b20iLCJmbG9hdCIsImN1cnNvciIsIndyYXBwZXIiLCJwcm9wcyIsImNvbnRleHQiLCJvblJlc2l6ZWRDaGFuZ2UiLCJyZXNpemVkIiwic2V0U3RhdGUiLCJyZW1vdmVSZXNpemVkIiwiaWQiLCJjb2x1bW4iLCJzdGF0ZSIsInJzIiwiZmluZCIsInIiLCJuZXdSZXNpemVkIiwiZmlsdGVyIiwiZ2V0V3JhcHBlZEluc3RhbmNlIiwid3JhcHBlZEluc3RhbmNlIiwiY29uc29sZSIsIndhcm4iLCJnZXRDb3BpZWRLZXkiLCJmb2xkYWJsZU9yaWdpbmFsS2V5Iiwia2V5IiwiY29weU9yaWdpbmFscyIsIkZvbGRlZENvbHVtbiIsIm9yaWdpbmFsX0hlYWRlciIsIk9iamVjdCIsImtleXMiLCJmb3JFYWNoIiwiY29waWVkS2V5IiwiayIsImMiLCJ2YWx1ZSIsImNvbHVtbnMiLCJvcmlnaW5hbF9Db2x1bW5zIiwiSGVhZGVyIiwicmVzdG9yZVRvT3JpZ2luYWwiLCJnZXRTdGF0ZSIsIm9uRm9sZENoYW5nZSIsImZvbGRlZCIsImlzRm9sZGVkIiwiY29sIiwiZm9sZGluZ0hhbmRsZXIiLCJuZXdGb2xkIiwiYXNzaWduIiwiZm9sZGFibGVIZWFkZXJSZW5kZXIiLCJGb2xkQnV0dG9uQ29tcG9uZW50IiwiRm9sZEljb25Db21wb25lbnQiLCJjZWxsIiwiUmVhY3QiLCJjcmVhdGVFbGVtZW50IiwiYXBwbHlGb2xkYWJsZUZvckNvbHVtbiIsImFwcGx5Rm9sZGFibGVGb3JDb2x1bW5zIiwibWFwIiwiaW5kZXgiLCJmb2xkYWJsZSIsInVuZGVmaW5lZCIsIm5ld1Byb3BzIiwib3JpZ2luYWxDb2xzIiwicmVzdCIsImV4dHJhIiwiQ29tcG9uZW50IiwiZGlzcGxheU5hbWUiLCJkZWZhdWx0UHJvcHMiLCJDZWxsIiwic29ydGFibGUiLCJyZXNpemFibGUiLCJmaWx0ZXJhYmxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7Ozs7Ozs7OzsrZUFKQTs7QUFNQSxJQUFNQSwyQkFBMkIsU0FBM0JBLHdCQUEyQixPQUEyQjtBQUFBLE1BQXhCQyxTQUF3QixRQUF4QkEsU0FBd0I7QUFBQSxNQUFiQyxNQUFhLFFBQWJBLE1BQWE7O0FBQzFELE1BQU1DLFFBQVEsRUFBRUMsT0FBTyxFQUFULEVBQWQ7O0FBRUEsTUFBSUgsU0FBSixFQUFlLE9BQU8sdUNBQUssS0FBS0ksZUFBVixFQUFpQixPQUFPRixLQUF4QixFQUErQixLQUFJLE9BQW5DLEVBQTJDLE9BQU9ELE1BQWxELEdBQVA7QUFDZixTQUFPLHVDQUFLLEtBQUtJLGNBQVYsRUFBZ0IsT0FBT0gsS0FBdkIsRUFBOEIsS0FBSSxNQUFsQyxHQUFQO0FBQ0QsQ0FMRDs7QUFPQSxJQUFNSSw2QkFBNkIsU0FBN0JBLDBCQUE2QixRQUEwQztBQUFBLE1BQXZDTCxNQUF1QyxTQUF2Q0EsTUFBdUM7QUFBQSxNQUEvQkQsU0FBK0IsU0FBL0JBLFNBQStCO0FBQUEsTUFBcEJPLElBQW9CLFNBQXBCQSxJQUFvQjtBQUFBLE1BQWRDLE9BQWMsU0FBZEEsT0FBYzs7QUFDM0UsTUFBTU4sUUFBUTtBQUNaTyxnQkFBWSxLQURBO0FBRVpDLGVBQVcsTUFGQztBQUdaQyxrQkFBYyxNQUhGO0FBSVpDLFdBQU8sTUFKSztBQUtaQyxZQUFRO0FBTEksR0FBZDs7QUFRQSxTQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxRQUFLLE9BQU9YLEtBQVosRUFBbUIsU0FBU00sT0FBNUI7QUFDR0Q7QUFESCxLQURGO0FBSUcsS0FBQ1AsU0FBRCxJQUFjO0FBQUE7QUFBQSxRQUFLLE9BQU8sRUFBQyxhQUFhLE1BQWQsRUFBWjtBQUFxQ0M7QUFBckM7QUFKakIsR0FERjtBQVFELENBakJEOztrQkFtQmUsc0JBQWM7QUFDM0IsTUFBTWE7QUFBQTs7QUFDSiw2QkFBWUMsS0FBWixFQUFtQkMsT0FBbkIsRUFBNEI7QUFBQTs7QUFBQSxvSUFDcEJELEtBRG9CLEVBQ2JDLE9BRGE7O0FBQUEsWUFlNUJDLGVBZjRCLEdBZVYsbUJBQVc7QUFBQSxZQUNuQkEsZUFEbUIsR0FDQyxNQUFLRixLQUROLENBQ25CRSxlQURtQjs7QUFFM0IsWUFBSUEsZUFBSixFQUFxQkEsZ0JBQWdCQyxPQUFoQixFQUFyQixLQUNLO0FBQ0gsZ0JBQUtDLFFBQUwsQ0FBYztBQUFBLG1CQUFNLEVBQUVELGdCQUFGLEVBQU47QUFBQSxXQUFkO0FBQ0Q7QUFDRixPQXJCMkI7O0FBQUEsWUF1QjVCRSxhQXZCNEIsR0F1Qlosa0JBQVU7QUFBQSxZQUNoQkMsRUFEZ0IsR0FDVEMsTUFEUyxDQUNoQkQsRUFEZ0I7O0FBRXhCLFlBQUksQ0FBQ0EsRUFBTCxFQUFTOztBQUZlLFlBSWhCSCxPQUpnQixHQUlKLE1BQUtLLEtBSkQsQ0FJaEJMLE9BSmdCOztBQUt4QixZQUFJLENBQUNBLE9BQUwsRUFBYzs7QUFFZCxZQUFNTSxLQUFLTixRQUFRTyxJQUFSLENBQWE7QUFBQSxpQkFBS0MsRUFBRUwsRUFBRixLQUFTQSxFQUFkO0FBQUEsU0FBYixDQUFYO0FBQ0EsWUFBSSxDQUFDRyxFQUFMLEVBQVM7O0FBRVQsWUFBTUcsYUFBYVQsUUFBUVUsTUFBUixDQUFlO0FBQUEsaUJBQUtGLE1BQU1GLEVBQVg7QUFBQSxTQUFmLENBQW5CO0FBQ0EsY0FBS1AsZUFBTCxDQUFxQlUsVUFBckI7QUFDRCxPQW5DMkI7O0FBQUEsWUFzQzVCRSxrQkF0QzRCLEdBc0NQLFlBQU07QUFDekIsWUFBSSxDQUFDLE1BQUtDLGVBQVYsRUFBMkJDLFFBQVFDLElBQVIsQ0FBYSx1Q0FBYjtBQUMzQixZQUFJLE1BQUtGLGVBQUwsQ0FBcUJELGtCQUF6QixFQUE2QyxPQUFPLE1BQUtDLGVBQUwsQ0FBcUJELGtCQUFyQixFQUFQO0FBQzdDLGVBQU8sTUFBS0MsZUFBWjtBQUNELE9BMUMyQjs7QUFBQSxZQTRDNUJHLFlBNUM0QixHQTRDYixlQUFPO0FBQUEsWUFDWkMsbUJBRFksR0FDWSxNQUFLbkIsS0FEakIsQ0FDWm1CLG1CQURZOztBQUVwQixvQkFBVUEsbUJBQVYsR0FBZ0NDLEdBQWhDO0FBQ0QsT0EvQzJCOztBQUFBLFlBaUQ1QkMsYUFqRDRCLEdBaURaLGtCQUFVO0FBQUEsWUFDaEJDLFlBRGdCLEdBQ0MsTUFBS3RCLEtBRE4sQ0FDaEJzQixZQURnQjs7QUFHeEI7O0FBQ0EsWUFBSWYsT0FBT2dCLGVBQVgsRUFBNEI7O0FBRTVCQyxlQUFPQyxJQUFQLENBQVlILFlBQVosRUFBMEJJLE9BQTFCLENBQWtDLGFBQUs7QUFDckMsY0FBTUMsWUFBWSxNQUFLVCxZQUFMLENBQWtCVSxDQUFsQixDQUFsQjs7QUFFQSxjQUFJQSxNQUFNLE1BQVYsRUFBa0JyQixPQUFPb0IsU0FBUCxJQUFvQnBCLE9BQU9xQixDQUFQLElBQVlyQixPQUFPcUIsQ0FBUCxDQUFaLEdBQXdCO0FBQUEsbUJBQUtDLEVBQUVDLEtBQVA7QUFBQSxXQUE1QyxDQUFsQixLQUNLdkIsT0FBT29CLFNBQVAsSUFBb0JwQixPQUFPcUIsQ0FBUCxDQUFwQjtBQUNOLFNBTEQ7O0FBT0E7QUFDQSxZQUFJckIsT0FBT3dCLE9BQVAsSUFBa0IsQ0FBQ3hCLE9BQU95QixnQkFBOUIsRUFBZ0R6QixPQUFPeUIsZ0JBQVAsR0FBMEJ6QixPQUFPd0IsT0FBakM7O0FBRWhEO0FBQ0EsWUFBSSxDQUFDeEIsT0FBT2dCLGVBQVosRUFBNkJoQixPQUFPZ0IsZUFBUCxHQUF5QmhCLE9BQU8wQixNQUFoQztBQUM5QixPQW5FMkI7O0FBQUEsWUFxRTVCQyxpQkFyRTRCLEdBcUVSLGtCQUFVO0FBQUEsWUFDcEJaLFlBRG9CLEdBQ0gsTUFBS3RCLEtBREYsQ0FDcEJzQixZQURvQjs7O0FBRzVCRSxlQUFPQyxJQUFQLENBQVlILFlBQVosRUFBMEJJLE9BQTFCLENBQWtDLGFBQUs7QUFDckM7QUFDQSxjQUFJRSxNQUFNLFFBQVYsRUFBb0I7O0FBRXBCLGNBQU1ELFlBQVksTUFBS1QsWUFBTCxDQUFrQlUsQ0FBbEIsQ0FBbEI7QUFDQXJCLGlCQUFPcUIsQ0FBUCxJQUFZckIsT0FBT29CLFNBQVAsQ0FBWjtBQUNELFNBTkQ7O0FBUUEsWUFBSXBCLE9BQU93QixPQUFQLElBQWtCeEIsT0FBT3lCLGdCQUE3QixFQUErQ3pCLE9BQU93QixPQUFQLEdBQWlCeEIsT0FBT3lCLGdCQUF4QjtBQUNoRCxPQWpGMkI7O0FBQUEsWUFtRjVCRyxRQW5GNEIsR0FtRmpCO0FBQUEsZUFBTyxNQUFLbkMsS0FBTCxDQUFXb0MsWUFBWCxHQUEwQixNQUFLcEMsS0FBTCxDQUFXcUMsTUFBckMsR0FBOEMsTUFBSzdCLEtBQUwsQ0FBVzZCLE1BQWhFO0FBQUEsT0FuRmlCOztBQUFBLFlBcUY1QkMsUUFyRjRCLEdBcUZqQixlQUFPO0FBQ2hCLFlBQU1ELFNBQVMsTUFBS0YsUUFBTCxFQUFmO0FBQ0EsZUFBT0UsT0FBT0UsSUFBSWpDLEVBQVgsTUFBbUIsSUFBMUI7QUFDRCxPQXhGMkI7O0FBQUEsWUEwRjVCa0MsY0ExRjRCLEdBMEZYLGVBQU87QUFDdEIsWUFBSSxDQUFDRCxHQUFELElBQVEsQ0FBQ0EsSUFBSWpDLEVBQWpCLEVBQXFCOztBQURDLFlBR2Q4QixZQUhjLEdBR0csTUFBS3BDLEtBSFIsQ0FHZG9DLFlBSGM7O0FBSXRCLFlBQU1DLFNBQVMsTUFBS0YsUUFBTCxFQUFmO0FBSnNCLFlBS2Q3QixFQUxjLEdBS1BpQyxHQUxPLENBS2RqQyxFQUxjOzs7QUFPdEIsWUFBTW1DLFVBQVVqQixPQUFPa0IsTUFBUCxDQUFjLEVBQWQsRUFBa0JMLE1BQWxCLENBQWhCO0FBQ0FJLGdCQUFRbkMsRUFBUixJQUFjLENBQUNtQyxRQUFRbkMsRUFBUixDQUFmOztBQUVBO0FBQ0EsY0FBS0QsYUFBTCxDQUFtQmtDLEdBQW5COztBQUVBLFlBQUlILFlBQUosRUFBa0JBLGFBQWFLLE9BQWIsRUFBbEIsS0FDSztBQUNILGdCQUFLckMsUUFBTCxDQUFjO0FBQUEsbUJBQWEsRUFBRWlDLFFBQVFJLE9BQVYsRUFBYjtBQUFBLFdBQWQ7QUFDRDtBQUNGLE9BM0cyQjs7QUFBQSxZQTZHNUJFLG9CQTdHNEIsR0E2R0wsZ0JBQVE7QUFBQSwwQkFDc0IsTUFBSzNDLEtBRDNCO0FBQUEsWUFDckI0QyxtQkFEcUIsZUFDckJBLG1CQURxQjtBQUFBLFlBQ0FDLGlCQURBLGVBQ0FBLGlCQURBO0FBQUEsWUFFckJ0QyxNQUZxQixHQUVWdUMsSUFGVSxDQUVyQnZDLE1BRnFCOztBQUc3QixZQUFNckIsU0FBU3FCLE9BQU9nQixlQUF0QjtBQUNBLFlBQU10QyxZQUFZLE1BQUtxRCxRQUFMLENBQWMvQixNQUFkLENBQWxCO0FBQ0EsWUFBTWYsT0FBT3VELGdCQUFNQyxhQUFOLENBQW9CSCxpQkFBcEIsRUFBdUMsRUFBRTVELG9CQUFGLEVBQWFDLGNBQWIsRUFBdkMsQ0FBYjtBQUNBLFlBQU1PLFVBQVUsU0FBVkEsT0FBVTtBQUFBLGlCQUFNLE1BQUsrQyxjQUFMLENBQW9CakMsTUFBcEIsQ0FBTjtBQUFBLFNBQWhCOztBQUVBLGVBQU93QyxnQkFBTUMsYUFBTixDQUFvQkosbUJBQXBCLEVBQXlDO0FBQzlDMUQsa0JBQVFBLE1BRHNDO0FBRTlDRCw4QkFGOEM7QUFHOUNPLG9CQUg4QztBQUk5Q0M7QUFKOEMsU0FBekMsQ0FBUDtBQU1ELE9BM0gyQjs7QUFBQSxZQTZINUJ3RCxzQkE3SDRCLEdBNkhILGtCQUFVO0FBQ2pDLFlBQU1oRSxZQUFZLE1BQUtxRCxRQUFMLENBQWMvQixNQUFkLENBQWxCO0FBRGlDLFlBRXpCZSxZQUZ5QixHQUVSLE1BQUt0QixLQUZHLENBRXpCc0IsWUFGeUI7O0FBSWpDOztBQUNBLFlBQUlmLE9BQU93QixPQUFYLEVBQW9CO0FBQ2xCLGNBQUk5QyxTQUFKLEVBQWU7QUFDYnNCLG1CQUFPd0IsT0FBUCxHQUFpQixDQUFDVCxZQUFELENBQWpCO0FBQ0FmLG1CQUFPbkIsS0FBUCxHQUFla0MsYUFBYWxDLEtBQTVCO0FBQ0FtQixtQkFBT3BCLEtBQVAsR0FBZW1DLGFBQWFuQyxLQUE1QjtBQUNELFdBSkQsTUFJTyxNQUFLK0MsaUJBQUwsQ0FBdUIzQixNQUF2QjtBQUNSO0FBQ0Q7QUFQQSxhQVFLLElBQUl0QixTQUFKLEVBQWVzQixTQUFTaUIsT0FBT2tCLE1BQVAsQ0FBY25DLE1BQWQsRUFBc0JlLFlBQXRCLENBQVQsQ0FBZixLQUNBO0FBQ0gsa0JBQUtZLGlCQUFMLENBQXVCM0IsTUFBdkI7QUFDRDtBQUNGLE9BOUkyQjs7QUFBQSxZQWdKNUIyQyx1QkFoSjRCLEdBZ0pGO0FBQUEsZUFDeEJuQixRQUFRb0IsR0FBUixDQUFZLFVBQUNaLEdBQUQsRUFBTWEsS0FBTixFQUFnQjtBQUMxQixjQUFJLENBQUNiLElBQUljLFFBQVQsRUFBbUIsT0FBT2QsR0FBUDs7QUFFbkI7QUFDQSxjQUFJLENBQUNBLElBQUlqQyxFQUFULEVBQWFpQyxJQUFJakMsRUFBSixZQUFnQjhDLEtBQWhCOztBQUViLGdCQUFLL0IsYUFBTCxDQUFtQmtCLEdBQW5CO0FBQ0E7QUFDQUEsY0FBSU4sTUFBSixHQUFhO0FBQUEsbUJBQUssTUFBS1Usb0JBQUwsQ0FBMEJkLENBQTFCLENBQUw7QUFBQSxXQUFiO0FBQ0E7QUFDQSxnQkFBS29CLHNCQUFMLENBQTRCVixHQUE1Qjs7QUFFQTtBQUNBLGlCQUFPQSxHQUFQO0FBQ0QsU0FkRCxDQUR3QjtBQUFBLE9BaEpFOztBQUcxQixZQUFLL0IsS0FBTCxHQUFhO0FBQ1g2QixnQkFBUXJDLE1BQU1vQyxZQUFOLEdBQXFCa0IsU0FBckIsR0FBaUMsRUFEOUI7QUFFWG5ELGlCQUFTSCxNQUFNRyxPQUFOLElBQWlCO0FBRmYsT0FBYjtBQUgwQjtBQU8zQjs7QUFSRztBQUFBO0FBQUEsZ0RBVXNCb0QsUUFWdEIsRUFVZ0M7QUFDbEMsWUFBSSxLQUFLL0MsS0FBTCxDQUFXTCxPQUFYLEtBQXVCb0QsU0FBU3BELE9BQXBDLEVBQTZDO0FBQzNDLGVBQUtDLFFBQUwsQ0FBYztBQUFBLG1CQUFNLEVBQUVELFNBQVNvRCxTQUFTcEQsT0FBcEIsRUFBTjtBQUFBLFdBQWQ7QUFDRDtBQUNGOztBQXdCRDs7QUF0Q0k7QUFBQTtBQUFBLCtCQWtLSztBQUFBOztBQUFBLHFCQU9ILEtBQUtILEtBUEY7QUFBQSxZQUVJd0QsWUFGSixVQUVMekIsT0FGSztBQUFBLFlBR0xhLG1CQUhLLFVBR0xBLG1CQUhLO0FBQUEsWUFJTEMsaUJBSkssVUFJTEEsaUJBSks7QUFBQSxZQUtMdkIsWUFMSyxVQUtMQSxZQUxLO0FBQUEsWUFNRm1DLElBTkU7O0FBUVAsWUFBTTFCLFVBQVUsS0FBS21CLHVCQUFMLDhCQUFpQ00sWUFBakMsR0FBaEI7O0FBRUEsWUFBTUUsUUFBUTtBQUNaM0IsMEJBRFk7QUFFWjdCLDJCQUFpQixLQUFLQSxlQUZWO0FBR1pDLG1CQUFTLEtBQUtLLEtBQUwsQ0FBV0w7QUFIUixTQUFkOztBQU1BLGVBQU8sOEJBQUMsVUFBRCxlQUFnQnNELElBQWhCLEVBQTBCQyxLQUExQixJQUFpQyxLQUFLO0FBQUEsbUJBQU0sT0FBSzNDLGVBQUwsR0FBdUJKLENBQTdCO0FBQUEsV0FBdEMsSUFBUDtBQUNEO0FBbkxHOztBQUFBO0FBQUEsSUFBd0NvQyxnQkFBTVksU0FBOUMsQ0FBTjs7QUFzTEE1RCxVQUFRNkQsV0FBUixHQUFzQixpQkFBdEI7QUFDQTdELFVBQVE4RCxZQUFSLEdBQXVCO0FBQ3JCaEIsdUJBQW1CN0Qsd0JBREU7QUFFckI0RCx5QkFBcUJyRCwwQkFGQTtBQUdyQjRCLHlCQUFxQixXQUhBO0FBSXJCRyxrQkFBYztBQUNad0MsWUFBTTtBQUFBLGVBQUssRUFBTDtBQUFBLE9BRE07QUFFWjFFLGFBQU8sRUFGSztBQUdaMkUsZ0JBQVUsS0FIRTtBQUlaQyxpQkFBVyxLQUpDO0FBS1pDLGtCQUFZO0FBTEE7QUFKTyxHQUF2Qjs7QUFhQSxTQUFPbEUsT0FBUDtBQUNELEMiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZGlzYWJsZSAqL1xyXG5cclxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgbGVmdCBmcm9tICcuL2xlZnQuc3ZnJ1xyXG5pbXBvcnQgcmlnaHQgZnJvbSAnLi9yaWdodC5zdmcnXHJcblxyXG5jb25zdCBkZWZhdWx0Rm9sZEljb25Db21wb25lbnQgPSAoeyBjb2xsYXBzZWQsIGhlYWRlciB9KSA9PiB7XHJcbiAgY29uc3Qgc3R5bGUgPSB7IHdpZHRoOiAyNSB9XHJcblxyXG4gIGlmIChjb2xsYXBzZWQpIHJldHVybiA8aW1nIHNyYz17cmlnaHR9IHN0eWxlPXtzdHlsZX0gYWx0PVwicmlnaHRcIiB0aXRsZT17aGVhZGVyfSAvPlxyXG4gIHJldHVybiA8aW1nIHNyYz17bGVmdH0gc3R5bGU9e3N0eWxlfSBhbHQ9XCJsZWZ0XCIgLz5cclxufVxyXG5cclxuY29uc3QgZGVmYXVsdEZvbGRCdXR0b25Db21wb25lbnQgPSAoeyBoZWFkZXIsIGNvbGxhcHNlZCwgaWNvbiwgb25DbGljayB9KSA9PiB7XHJcbiAgY29uc3Qgc3R5bGUgPSB7XHJcbiAgICBtYXJnaW5MZWZ0OiAnMHB4JyxcclxuICAgIG1hcmdpblRvcDogJy01cHgnLFxyXG4gICAgbWFyZ2luQm90dG9tOiAnLThweCcsXHJcbiAgICBmbG9hdDogJ2xlZnQnLFxyXG4gICAgY3Vyc29yOiAncG9pbnRlcicsXHJcbiAgfVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPGRpdj5cclxuICAgICAgPGRpdiBzdHlsZT17c3R5bGV9IG9uQ2xpY2s9e29uQ2xpY2t9PlxyXG4gICAgICAgIHtpY29ufVxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgeyFjb2xsYXBzZWQgJiYgPGRpdiBzdHlsZT17e1widGV4dEFsaWduXCI6IFwibGVmdFwifX0gPntoZWFkZXJ9PC9kaXY+fVxyXG4gICAgPC9kaXY+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBSZWFjdFRhYmxlID0+IHtcclxuICBjb25zdCB3cmFwcGVyID0gY2xhc3MgUlRGb2xkYWJsZVRhYmxlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKHByb3BzLCBjb250ZXh0KSB7XHJcbiAgICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KVxyXG5cclxuICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICBmb2xkZWQ6IHByb3BzLm9uRm9sZENoYW5nZSA/IHVuZGVmaW5lZCA6IHt9LFxyXG4gICAgICAgIHJlc2l6ZWQ6IHByb3BzLnJlc2l6ZWQgfHwgW10sXHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5ld1Byb3BzKSB7XHJcbiAgICAgIGlmICh0aGlzLnN0YXRlLnJlc2l6ZWQgIT09IG5ld1Byb3BzLnJlc2l6ZWQpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHAgPT4gKHsgcmVzaXplZDogbmV3UHJvcHMucmVzaXplZCB9KSlcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmVzaXplZENoYW5nZSA9IHJlc2l6ZWQgPT4ge1xyXG4gICAgICBjb25zdCB7IG9uUmVzaXplZENoYW5nZSB9ID0gdGhpcy5wcm9wc1xyXG4gICAgICBpZiAob25SZXNpemVkQ2hhbmdlKSBvblJlc2l6ZWRDaGFuZ2UocmVzaXplZClcclxuICAgICAgZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZShwID0+ICh7IHJlc2l6ZWQgfSkpXHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVSZXNpemVkID0gY29sdW1uID0+IHtcclxuICAgICAgY29uc3QgeyBpZCB9ID0gY29sdW1uXHJcbiAgICAgIGlmICghaWQpIHJldHVyblxyXG5cclxuICAgICAgY29uc3QgeyByZXNpemVkIH0gPSB0aGlzLnN0YXRlXHJcbiAgICAgIGlmICghcmVzaXplZCkgcmV0dXJuXHJcblxyXG4gICAgICBjb25zdCBycyA9IHJlc2l6ZWQuZmluZChyID0+IHIuaWQgPT09IGlkKVxyXG4gICAgICBpZiAoIXJzKSByZXR1cm5cclxuXHJcbiAgICAgIGNvbnN0IG5ld1Jlc2l6ZWQgPSByZXNpemVkLmZpbHRlcihyID0+IHIgIT09IHJzKVxyXG4gICAgICB0aGlzLm9uUmVzaXplZENoYW5nZShuZXdSZXNpemVkKVxyXG4gICAgfVxyXG5cclxuICAgIC8vIHRoaXMgaXMgc28gd2UgY2FuIGV4cG9zZSB0aGUgdW5kZXJseWluZyBSZWFjdFRhYmxlLlxyXG4gICAgZ2V0V3JhcHBlZEluc3RhbmNlID0gKCkgPT4ge1xyXG4gICAgICBpZiAoIXRoaXMud3JhcHBlZEluc3RhbmNlKSBjb25zb2xlLndhcm4oJ1JURm9sZGFibGVUYWJsZSAtIE5vIHdyYXBwZWQgaW5zdGFuY2UnKVxyXG4gICAgICBpZiAodGhpcy53cmFwcGVkSW5zdGFuY2UuZ2V0V3JhcHBlZEluc3RhbmNlKSByZXR1cm4gdGhpcy53cmFwcGVkSW5zdGFuY2UuZ2V0V3JhcHBlZEluc3RhbmNlKClcclxuICAgICAgcmV0dXJuIHRoaXMud3JhcHBlZEluc3RhbmNlXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29waWVkS2V5ID0ga2V5ID0+IHtcclxuICAgICAgY29uc3QgeyBmb2xkYWJsZU9yaWdpbmFsS2V5IH0gPSB0aGlzLnByb3BzXHJcbiAgICAgIHJldHVybiBgJHtmb2xkYWJsZU9yaWdpbmFsS2V5fSR7a2V5fWBcclxuICAgIH1cclxuXHJcbiAgICBjb3B5T3JpZ2luYWxzID0gY29sdW1uID0+IHtcclxuICAgICAgY29uc3QgeyBGb2xkZWRDb2x1bW4gfSA9IHRoaXMucHJvcHNcclxuXHJcbiAgICAgIC8vIFN0b3AgY29weSBpZiB0aGUgY29sdW1uIGFscmVhZHkgY29waWVkXHJcbiAgICAgIGlmIChjb2x1bW4ub3JpZ2luYWxfSGVhZGVyKSByZXR1cm5cclxuXHJcbiAgICAgIE9iamVjdC5rZXlzKEZvbGRlZENvbHVtbikuZm9yRWFjaChrID0+IHtcclxuICAgICAgICBjb25zdCBjb3BpZWRLZXkgPSB0aGlzLmdldENvcGllZEtleShrKVxyXG5cclxuICAgICAgICBpZiAoayA9PT0gJ0NlbGwnKSBjb2x1bW5bY29waWVkS2V5XSA9IGNvbHVtbltrXSA/IGNvbHVtbltrXSA6IGMgPT4gYy52YWx1ZVxyXG4gICAgICAgIGVsc2UgY29sdW1uW2NvcGllZEtleV0gPSBjb2x1bW5ba11cclxuICAgICAgfSlcclxuXHJcbiAgICAgIC8vIENvcHkgc3ViIENvbHVtbnNcclxuICAgICAgaWYgKGNvbHVtbi5jb2x1bW5zICYmICFjb2x1bW4ub3JpZ2luYWxfQ29sdW1ucykgY29sdW1uLm9yaWdpbmFsX0NvbHVtbnMgPSBjb2x1bW4uY29sdW1uc1xyXG5cclxuICAgICAgLy8gQ29weSBIZWFkZXJcclxuICAgICAgaWYgKCFjb2x1bW4ub3JpZ2luYWxfSGVhZGVyKSBjb2x1bW4ub3JpZ2luYWxfSGVhZGVyID0gY29sdW1uLkhlYWRlclxyXG4gICAgfVxyXG5cclxuICAgIHJlc3RvcmVUb09yaWdpbmFsID0gY29sdW1uID0+IHtcclxuICAgICAgY29uc3QgeyBGb2xkZWRDb2x1bW4gfSA9IHRoaXMucHJvcHNcclxuXHJcbiAgICAgIE9iamVjdC5rZXlzKEZvbGRlZENvbHVtbikuZm9yRWFjaChrID0+IHtcclxuICAgICAgICAvLyBpZ25vcmUgaGVhZGVyIGFzIGhhbmRsaW5nIGJ5IGZvbGRhYmxlSGVhZGVyUmVuZGVyXHJcbiAgICAgICAgaWYgKGsgPT09ICdIZWFkZXInKSByZXR1cm5cclxuXHJcbiAgICAgICAgY29uc3QgY29waWVkS2V5ID0gdGhpcy5nZXRDb3BpZWRLZXkoaylcclxuICAgICAgICBjb2x1bW5ba10gPSBjb2x1bW5bY29waWVkS2V5XVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaWYgKGNvbHVtbi5jb2x1bW5zICYmIGNvbHVtbi5vcmlnaW5hbF9Db2x1bW5zKSBjb2x1bW4uY29sdW1ucyA9IGNvbHVtbi5vcmlnaW5hbF9Db2x1bW5zXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U3RhdGUgPSAoKSA9PiAodGhpcy5wcm9wcy5vbkZvbGRDaGFuZ2UgPyB0aGlzLnByb3BzLmZvbGRlZCA6IHRoaXMuc3RhdGUuZm9sZGVkKVxyXG5cclxuICAgIGlzRm9sZGVkID0gY29sID0+IHtcclxuICAgICAgY29uc3QgZm9sZGVkID0gdGhpcy5nZXRTdGF0ZSgpXHJcbiAgICAgIHJldHVybiBmb2xkZWRbY29sLmlkXSA9PT0gdHJ1ZVxyXG4gICAgfVxyXG5cclxuICAgIGZvbGRpbmdIYW5kbGVyID0gY29sID0+IHtcclxuICAgICAgaWYgKCFjb2wgfHwgIWNvbC5pZCkgcmV0dXJuXHJcblxyXG4gICAgICBjb25zdCB7IG9uRm9sZENoYW5nZSB9ID0gdGhpcy5wcm9wc1xyXG4gICAgICBjb25zdCBmb2xkZWQgPSB0aGlzLmdldFN0YXRlKClcclxuICAgICAgY29uc3QgeyBpZCB9ID0gY29sXHJcblxyXG4gICAgICBjb25zdCBuZXdGb2xkID0gT2JqZWN0LmFzc2lnbih7fSwgZm9sZGVkKVxyXG4gICAgICBuZXdGb2xkW2lkXSA9ICFuZXdGb2xkW2lkXVxyXG5cclxuICAgICAgLy8gUmVtb3ZlIHRoZSBSZXNpemVkIGlmIGhhdmVcclxuICAgICAgdGhpcy5yZW1vdmVSZXNpemVkKGNvbClcclxuXHJcbiAgICAgIGlmIChvbkZvbGRDaGFuZ2UpIG9uRm9sZENoYW5nZShuZXdGb2xkKVxyXG4gICAgICBlbHNlIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHByZXZpb3VzID0+ICh7IGZvbGRlZDogbmV3Rm9sZCB9KSlcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvbGRhYmxlSGVhZGVyUmVuZGVyID0gY2VsbCA9PiB7XHJcbiAgICAgIGNvbnN0IHsgRm9sZEJ1dHRvbkNvbXBvbmVudCwgRm9sZEljb25Db21wb25lbnQgfSA9IHRoaXMucHJvcHNcclxuICAgICAgY29uc3QgeyBjb2x1bW4gfSA9IGNlbGxcclxuICAgICAgY29uc3QgaGVhZGVyID0gY29sdW1uLm9yaWdpbmFsX0hlYWRlcjtcclxuICAgICAgY29uc3QgY29sbGFwc2VkID0gdGhpcy5pc0ZvbGRlZChjb2x1bW4pXHJcbiAgICAgIGNvbnN0IGljb24gPSBSZWFjdC5jcmVhdGVFbGVtZW50KEZvbGRJY29uQ29tcG9uZW50LCB7IGNvbGxhcHNlZCwgaGVhZGVyICB9KVxyXG4gICAgICBjb25zdCBvbkNsaWNrID0gKCkgPT4gdGhpcy5mb2xkaW5nSGFuZGxlcihjb2x1bW4pXHJcblxyXG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChGb2xkQnV0dG9uQ29tcG9uZW50LCB7XHJcbiAgICAgICAgaGVhZGVyOiBoZWFkZXIsXHJcbiAgICAgICAgY29sbGFwc2VkLFxyXG4gICAgICAgIGljb24sXHJcbiAgICAgICAgb25DbGljayxcclxuICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBhcHBseUZvbGRhYmxlRm9yQ29sdW1uID0gY29sdW1uID0+IHtcclxuICAgICAgY29uc3QgY29sbGFwc2VkID0gdGhpcy5pc0ZvbGRlZChjb2x1bW4pXHJcbiAgICAgIGNvbnN0IHsgRm9sZGVkQ29sdW1uIH0gPSB0aGlzLnByb3BzXHJcblxyXG4gICAgICAvLyBIYW5kbGUgQ29sdW1uIEhlYWRlclxyXG4gICAgICBpZiAoY29sdW1uLmNvbHVtbnMpIHtcclxuICAgICAgICBpZiAoY29sbGFwc2VkKSB7XHJcbiAgICAgICAgICBjb2x1bW4uY29sdW1ucyA9IFtGb2xkZWRDb2x1bW5dXHJcbiAgICAgICAgICBjb2x1bW4ud2lkdGggPSBGb2xkZWRDb2x1bW4ud2lkdGhcclxuICAgICAgICAgIGNvbHVtbi5zdHlsZSA9IEZvbGRlZENvbHVtbi5zdHlsZVxyXG4gICAgICAgIH0gZWxzZSB0aGlzLnJlc3RvcmVUb09yaWdpbmFsKGNvbHVtbilcclxuICAgICAgfVxyXG4gICAgICAvLyBIYW5kbGUgTm9ybWFsIENvbHVtbi5cclxuICAgICAgZWxzZSBpZiAoY29sbGFwc2VkKSBjb2x1bW4gPSBPYmplY3QuYXNzaWduKGNvbHVtbiwgRm9sZGVkQ29sdW1uKVxyXG4gICAgICBlbHNlIHtcclxuICAgICAgICB0aGlzLnJlc3RvcmVUb09yaWdpbmFsKGNvbHVtbilcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFwcGx5Rm9sZGFibGVGb3JDb2x1bW5zID0gY29sdW1ucyA9PlxyXG4gICAgICBjb2x1bW5zLm1hcCgoY29sLCBpbmRleCkgPT4ge1xyXG4gICAgICAgIGlmICghY29sLmZvbGRhYmxlKSByZXR1cm4gY29sXHJcblxyXG4gICAgICAgIC8vIElmIGNvbCBkb24ndCBoYXZlIGlkIHRoZW4gZ2VuZXJhdGUgaWQgYmFzZWQgb24gaW5kZXhcclxuICAgICAgICBpZiAoIWNvbC5pZCkgY29sLmlkID0gYGNvbF8ke2luZGV4fWBcclxuXHJcbiAgICAgICAgdGhpcy5jb3B5T3JpZ2luYWxzKGNvbClcclxuICAgICAgICAvLyBSZXBsYWNlIGN1cnJlbnQgaGVhZGVyIHdpdGggaW50ZXJuYWwgaGVhZGVyIHJlbmRlci5cclxuICAgICAgICBjb2wuSGVhZGVyID0gYyA9PiB0aGlzLmZvbGRhYmxlSGVhZGVyUmVuZGVyKGMpXHJcbiAgICAgICAgLy8gYXBwbHkgZm9sZGFibGVcclxuICAgICAgICB0aGlzLmFwcGx5Rm9sZGFibGVGb3JDb2x1bW4oY29sKVxyXG5cclxuICAgICAgICAvLyByZXR1cm4gdGhlIG5ldyBjb2x1bW4gb3V0XHJcbiAgICAgICAgcmV0dXJuIGNvbFxyXG4gICAgICB9KVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgY29uc3Qge1xyXG4gICAgICAgIGNvbHVtbnM6IG9yaWdpbmFsQ29scyxcclxuICAgICAgICBGb2xkQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgICAgIEZvbGRJY29uQ29tcG9uZW50LFxyXG4gICAgICAgIEZvbGRlZENvbHVtbixcclxuICAgICAgICAuLi5yZXN0XHJcbiAgICAgIH0gPSB0aGlzLnByb3BzXHJcbiAgICAgIGNvbnN0IGNvbHVtbnMgPSB0aGlzLmFwcGx5Rm9sZGFibGVGb3JDb2x1bW5zKFsuLi5vcmlnaW5hbENvbHNdKVxyXG5cclxuICAgICAgY29uc3QgZXh0cmEgPSB7XHJcbiAgICAgICAgY29sdW1ucyxcclxuICAgICAgICBvblJlc2l6ZWRDaGFuZ2U6IHRoaXMub25SZXNpemVkQ2hhbmdlLFxyXG4gICAgICAgIHJlc2l6ZWQ6IHRoaXMuc3RhdGUucmVzaXplZCxcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIDxSZWFjdFRhYmxlIHsuLi5yZXN0fSB7Li4uZXh0cmF9IHJlZj17ciA9PiAodGhpcy53cmFwcGVkSW5zdGFuY2UgPSByKX0gLz5cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHdyYXBwZXIuZGlzcGxheU5hbWUgPSAnUlRGb2xkYWJsZVRhYmxlJ1xyXG4gIHdyYXBwZXIuZGVmYXVsdFByb3BzID0ge1xyXG4gICAgRm9sZEljb25Db21wb25lbnQ6IGRlZmF1bHRGb2xkSWNvbkNvbXBvbmVudCxcclxuICAgIEZvbGRCdXR0b25Db21wb25lbnQ6IGRlZmF1bHRGb2xkQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgZm9sZGFibGVPcmlnaW5hbEtleTogJ29yaWdpbmFsXycsXHJcbiAgICBGb2xkZWRDb2x1bW46IHtcclxuICAgICAgQ2VsbDogYyA9PiAnJyxcclxuICAgICAgd2lkdGg6IDMwLFxyXG4gICAgICBzb3J0YWJsZTogZmFsc2UsXHJcbiAgICAgIHJlc2l6YWJsZTogZmFsc2UsXHJcbiAgICAgIGZpbHRlcmFibGU6IGZhbHNlLFxyXG4gICAgfSxcclxuICB9XHJcblxyXG4gIHJldHVybiB3cmFwcGVyXHJcbn1cclxuIl19